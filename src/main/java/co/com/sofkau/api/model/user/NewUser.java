package co.com.sofkau.api.model.user;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NewUser {

    private String name;
    private String job;
    private String id;
    private String createdAt;
}
