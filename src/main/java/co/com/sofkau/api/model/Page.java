package co.com.sofkau.api.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Page {

    private String page;
    private String total;
}
