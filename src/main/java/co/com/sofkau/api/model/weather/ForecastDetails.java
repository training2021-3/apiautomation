package co.com.sofkau.api.model.weather;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ForecastDetails {

    private String day;
    private String temperature;
    private String wind;
}
