package co.com.sofkau.api.model.data;

import co.com.sofkau.api.model.user.NewUser;
import com.github.javafaker.Faker;

import java.security.SecureRandom;
import java.util.Locale;

public class UserData {

    private static Faker faker = Faker.instance(new Locale("es", "CO"), new SecureRandom());

    public static NewUser getNewUser() {
        return NewUser.builder()
                .name(faker.name().fullName())
                .job(faker.job().position())
                .build();
    }
}
