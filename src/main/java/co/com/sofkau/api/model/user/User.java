package co.com.sofkau.api.model.user;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User {

    private String id;
    private String email;
    private String first_name;
    private String last_name;
    private String avatar;
}
