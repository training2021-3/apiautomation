package co.com.sofkau.api.model.weather;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class WeatherGeneralData {

    private String temperature;
    private String wind;
    private String description;
}
