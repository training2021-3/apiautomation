#language: es

Característica: Gestión de usuarios en REQ-IN

  Escenario: Consulta exitosa de todos los usuarios
    Cuando un analista consulte todos los usuarios en REQIN
    Entonces puede recuperar la informacion de los usuarios correctamente

  Escenario: Consulta fallida usuario no existente
    Cuando un analista consulte un usuario no existente en REQIN
    Entonces se presenta el codigo de error "404"

  Escenario: Creacion exitosa de usuario nuevo
    Cuando se tiene la informacion de un nuevo usuario para crear en REQIN
    Entonces se crea el usuario exitosamente