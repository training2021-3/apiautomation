package screenplay;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.serenitybdd.screenplay.rest.interactions.Post;
import org.junit.Test;

public class TestUsers {

    @Test
    public void consultarTodosLosUsuarios() {
        Actor actor = Actor.named("Elkin");
        actor.whoCan(CallAnApi.at("https://reqres.in/"));
        actor.attemptsTo(Get.resource("api/users"));
        System.out.println("Response -> " + SerenityRest.lastResponse().print());
    }

    @Test
    public void consultarUsuarioNoExistente() {
        Actor actor = Actor.named("Elkin");
        actor.whoCan(CallAnApi.at("https://reqres.in/"));
        actor.attemptsTo(Get.resource("api/users/23"));
        System.out.println("Response -> " + SerenityRest.lastResponse().print());
    }


    @Test
    public void guardarUsuario() {
        Actor actor = Actor.named("Elkin");
        actor.whoCan(CallAnApi.at("https://reqres.in/"));
        String mensaje = "{\n" +
                "    \"name\": \"Elkin Piedrahita\",\n" +
                "    \"job\": \"Analista\"\n" +
                "}";
        actor.attemptsTo(Post.to("api/users").withRequest(request ->
                request.body(mensaje)));
        System.out.println("Response -> " + SerenityRest.lastResponse().print());
    }
}
