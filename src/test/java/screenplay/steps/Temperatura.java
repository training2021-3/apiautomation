package screenplay.steps;

import co.com.sofkau.api.model.weather.ForecastDetails;
import co.com.sofkau.api.model.weather.WeatherGeneralData;
import co.com.sofkau.api.task.GetRequest;
import io.cucumber.java.Before;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.assertj.core.api.Assertions.assertThat;

public class Temperatura {

    Actor actor = Actor.named("Elkin");
    EnvironmentVariables variables;

    @Before
    public void setup() {
        actor.whoCan(CallAnApi.at(variables.getProperty("baseUrlWeather")));
    }

    @Cuando("un analista consulte las condiciones metereologicas de {string}")
    public void unAnalistaConsulteLasCondicionesMetereologicasDe(String ciudad) {
        actor.attemptsTo(GetRequest.withResource(ciudad));
    }

    @Entonces("se entrega la informacion correspondiente para la ciudad")
    public void seEntregaLaInformacionCorrespondienteParaLaCiudad() {
        actor.should(seeThatResponse("ver el código de respuesta", response ->
                response.statusCode(200)));
        WeatherGeneralData generalData = SerenityRest.lastResponse()
                .jsonPath().getObject("", WeatherGeneralData.class);
        assertThat(generalData).hasNoNullFieldsOrProperties();
        ForecastDetails[] details = SerenityRest.lastResponse()
                .jsonPath().getObject("forecast", ForecastDetails[].class);
        for (int i = 0; i < details.length; i++) {
            assertThat(details[i]).hasNoNullFieldsOrProperties();
        }
    }
}
