package screenplay.steps;

import co.com.sofkau.api.model.Page;
import co.com.sofkau.api.model.data.UserData;
import co.com.sofkau.api.model.user.NewUser;
import co.com.sofkau.api.model.user.User;
import co.com.sofkau.api.task.GetRequest;
import co.com.sofkau.api.task.PostRequest;
import io.cucumber.java.Before;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.assertj.core.api.Assertions.assertThat;

public class UserSteps {

    Actor actor = Actor.named("Elkin");
    EnvironmentVariables variables;

    @Before
    public void setup() {
        actor.whoCan(CallAnApi.at(variables.getProperty("baseurl")));
    }

    @Cuando("un analista consulte todos los usuarios en REQIN")
    public void unAnalistaConsulteTodosLosUsuariosEnREQIN() {
        actor.attemptsTo(GetRequest.withResource(variables.getProperty("pathUser")));
    }

    @Entonces("puede recuperar la informacion de los usuarios correctamente")
    public void puedeRecuperarLaInformacionDeLosUsuariosCorrectamente() {
        actor.should(seeThatResponse("ver el código de respuesta",
                response -> response.statusCode(200)));
        Page page = SerenityRest.lastResponse()
                .jsonPath().getObject("", Page.class);
        assertThat(page).hasNoNullFieldsOrProperties();
        User[] users = SerenityRest.lastResponse()
                .jsonPath().getObject("data", User[].class);
        for (int i = 0; i < users.length; i++) {
            assertThat(users[i]).hasNoNullFieldsOrProperties();
        }
    }

    @Cuando("un analista consulte un usuario no existente en REQIN")
    public void unAnalistaConsulteUnUsuarioNoExistenteEnREQIN() {
        actor.attemptsTo(GetRequest.withResource(variables.getProperty("pathUser").concat("23")));
    }

    @Entonces("se presenta el codigo de error {string}")
    public void sePresentaElCodigoDeError(String codigoError) {
        actor.should(seeThatResponse("ver el código de respuesta",
                response -> response.statusCode(Integer.parseInt(codigoError))));
        assertThat(SerenityRest.lastResponse().print()).isEqualTo("{}");
    }

    @Cuando("se tiene la informacion de un nuevo usuario para crear en REQIN")
    public void seTieneLaInformacionDeUnNuevoUsuarioParaCrearEnREQIN() {
        actor.attemptsTo(PostRequest.withData(variables.getProperty("pathUser"), UserData.getNewUser()));
    }

    @Entonces("se crea el usuario exitosamente")
    public void seCreaElUsuarioExitosamente() {
        actor.should(seeThatResponse("ver el código de respuesta",
                response -> response.statusCode(201)));
        NewUser user = SerenityRest.lastResponse()
                .jsonPath().getObject("", NewUser.class);
        assertThat(user).hasNoNullFieldsOrProperties();
    }
}
